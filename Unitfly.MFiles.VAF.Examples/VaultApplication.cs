﻿using System;
using System.Collections.Generic;
using System.Linq;
using MFiles.VAF;
using MFiles.VAF.AdminConfigurations;
using MFiles.VAF.Common;
using MFilesAPI;
using Serilog;

namespace Unitfly.MFiles.VAF.Examples
{
    public class VaultApplication : VaultApplicationBase, IUsesAdminConfigurations
    {
        #region Config
        public ConfigurationNode<Configuration> Configuration { get; private set; }

        public void InitializeAdminConfigurations(IAdminConfigurations adminConfigurations)
        {
            Configuration = adminConfigurations.AddSimpleConfigurationNode<Configuration>("Unitfly VAF Demo");
        }
        #endregion

        protected override void StartApplication()
        {
            base.StartApplication();

            Log.Logger = new LoggerConfiguration()
                .WriteTo.File(@"C:/Unitfly/MFiles/VAF/Examples/log.txt", rollingInterval: RollingInterval.Day).MinimumLevel.Debug()
                .WriteTo.EventLog("Unitfly.MFiles.VAF.Examples", manageEventSource: true)
                .CreateLogger();

            base.BackgroundOperations.StartRecurringBackgroundOperation("Recurring Hello World Operation",
                TimeSpan.FromSeconds(10), () =>
                {
                    BackgroundOperation();
                });
        }

        private void BackgroundOperation()
        {
            if (Configuration.CurrentConfiguration?.Enabled != true ||
                Configuration.CurrentConfiguration.BackgroundOperation?.Enabled != true)
            {
                return;
            }

            SysUtils.ReportInfoToEventLog("Hello, world!");
            Log.Information("Hello, world");
        }

        #region Event Handlers
        [EventHandler(MFEventHandlerType.MFEventHandlerBeforeCheckInChanges)]
        public void AddClassKeyword(EventHandlerEnvironment env)
        {
            if (Configuration.CurrentConfiguration?.Enabled != true ||
                Configuration.CurrentConfiguration.EventHandlers?.Enabled != true)
            {
                return;
            }

            var keywords = env.ObjVerEx.GetPropertyText(MFBuiltInPropertyDef.MFBuiltInPropertyDefKeywords) ?? string.Empty;
            var objClassName = env.Vault.ClassOperations.GetObjectClass(env.ObjVerEx.Class)?.Name;
            if (!keywords.ToLower().Split(';').Contains(objClassName.ToLower()))
            {
                keywords += $";{objClassName}";
                keywords = keywords.Trim(';');
                env.ObjVerEx.SaveProperty(MFBuiltInPropertyDef.MFBuiltInPropertyDefKeywords, MFDataType.MFDatatypeText, keywords);
                env.ObjVerEx.SetModifiedBy(env.CurrentUserID);
            }
        }
        #endregion

        #region Workflows
        [StateAction("WS.Workflow.State")]
        public void DoSomethingStateAction(StateEnvironment env)
        {
            if (Configuration.CurrentConfiguration?.Enabled != true ||
                Configuration.CurrentConfiguration.Workflows?.Enabled != true)
            {
                return;
            }

            CreateObject(env.Vault, Configuration.CurrentConfiguration.Workflows.ObjType.ID, Configuration.CurrentConfiguration.Workflows.ObjClass.ID, Guid.NewGuid().ToString(), env.CurrentUserID);
        }
        #endregion

        #region Properties
        [PropertyCustomValue("PD.Today")]
        public TypedValue TodayCustomProperty(PropertyEnvironment env)
        {
            try
            {
                var val = new TypedValue();
                val.SetValue(MFDataType.MFDatatypeDate, DateTime.Today);
                return val;
            }
            catch (Exception e)
            {
                SysUtils.ReportWarningToEventLog("Unitfly.MFiles.VAF.Examples", "Error calculating property PD.Today", e);
                return null;
            }
        }

        [PropertyValueValidation("PD.Deadline")]
        public bool DeadlinePropertyValidation(PropertyEnvironment env, out string message)
        {
            message = "Deadline date must not be in the past.";
            var val = string.IsNullOrWhiteSpace(env.PropertyValue?.TypedValue?.DisplayValue) ? null : env.PropertyValue?.GetValue<DateTime?>();
            return (val.HasValue ? val.Value.Date >= DateTime.Today : true);
        }
        #endregion

        [VaultExtensionMethod("Search", RequiredVaultAccess = MFVaultAccess.MFVaultAccessNone)]
        private string SearchVaultExtensionMethod(EventHandlerEnvironment env)
        {
            var searchResult = SearchForDocuments(env.Vault, env.Input);
            return $"Search for '{env.Input}' returned {searchResult?.Count() ?? 0} documents.";
        }

        private IEnumerable<ObjVerEx> SearchForDocuments(Vault vault, string name)
        {
            var searchBuilder = new MFSearchBuilder(vault);
            searchBuilder.ObjType((int)MFBuiltInObjectType.MFBuiltInObjectTypeDocument);
            searchBuilder.Deleted(false);
            searchBuilder.Property(MFBuiltInPropertyDef.MFBuiltInPropertyDefNameOrTitle, MFDataType.MFDatatypeText, name);
            return searchBuilder.FindEx();
        }

        private ObjectVersion CreateObject(Vault vault, int objType, int objClass, string title, int currUserId, params PropertyValue[] properties)
        {
            var props = new PropertyValues();

            // Set class
            var classLookup = new Lookup()
            {
                ObjectType = objType,
                Item = objClass
            };
            var classTypedValue = new TypedValue();
            classTypedValue.SetValueToLookup(classLookup);
            var @class = new PropertyValue()
            {
                PropertyDef = (int)MFBuiltInPropertyDef.MFBuiltInPropertyDefClass,
                TypedValue = classTypedValue
            };

            // Set title
            var titleProp = new PropertyValue()
            {
                PropertyDef = (int)MFBuiltInPropertyDef.MFBuiltInPropertyDefNameOrTitle,
                TypedValue = new TypedValue()
                {
                    Value = title
                }
            };
            props.Add(-1, titleProp);

            // Set additional properties
            foreach (var prop in properties ?? new PropertyValue[0])
            {
                props.Add(-1, prop);
            }

            // Create object
            var newObj = vault.ObjectOperations.CreateNewObject(objType, props);
            new ObjVerEx(vault, newObj).SetCreatedBy(currUserId);
            return vault.ObjectOperations.CheckIn(newObj.ObjVer);
        }
    }
}