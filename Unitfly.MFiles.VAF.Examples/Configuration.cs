﻿using MFiles.VAF.Configuration;
using System.Runtime.Serialization;


namespace Unitfly.MFiles.VAF.Examples
{
    [DataContract]
    public class Configuration
    {
        [DataMember(Order = 1)]
        [JsonConfEditor(DefaultValue = false)]
        public bool Enabled { get; set; }

        [DataMember(Order = 2, Name = "Event Handlers")]
        public Settings EventHandlers { get; set; }

        [DataMember(Order = 3, Name = "Background Operations")]
        public Settings BackgroundOperation { get; set; }

        [DataMember(Order = 4, Name = "Workflows")]
        public WorkflowSettings Workflows { get; set; }
    }

    [DataContract]
    public class Settings
    {
        [DataMember(Order = 1)]
        [JsonConfEditor(DefaultValue = false)]
        public bool Enabled { get; set; }
    }

    [DataContract]
    public class WorkflowSettings
    {
        [DataMember(Order = 1)]
        [JsonConfEditor(DefaultValue = false)]
        public bool Enabled { get; set; }

        [DataMember(Order = 2, Name = "Object Type")]
        [MFObjType(AllowEmpty = false)]
        public MFIdentifier ObjType { get; set; }

        [DataMember(Order = 3, Name = "Object Class")]
        [MFClass(AllowEmpty = false)]
        public MFIdentifier ObjClass { get; set; }
    }
}
